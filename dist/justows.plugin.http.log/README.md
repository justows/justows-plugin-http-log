# justows.plugin.http.log

Plugin for generating a log entry for every HTTP request.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by Justo WebServices.*

## Description

This plugin provides a pre action for registering the start timestamp and a post action for issuing a log entry when finished.

## Use

The plugin returns a function which returns the plugin metadata:

```
function plugin(opts) : object
```

- `opts`:
  - `resource` (bool). Must the resource path be logged? Default: `false`.

Example:

```
http: {
  plugins: [
    require("justows.plugin.http.log")({resource: true})
  ],

  ...
}
```
