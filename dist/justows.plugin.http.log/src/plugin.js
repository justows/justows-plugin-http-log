"use strict";

var _dogmalang = require("dogmalang");

function plugin(opts) {
  _dogmalang.dogma.paramExpectedToHave("opts", opts, { rsc: { type: _dogmalang.bool, mandatory: false } }, false);{
    return { ["pre"]: resp => {
        _dogmalang.dogma.paramExpected("resp", resp, null);{
          Object.defineProperty(resp, "_ts", { value: (0, _dogmalang.datetime)() });
        }
      }, ["post"]: (req, resp, logger) => {
        _dogmalang.dogma.paramExpected("req", req, null);_dogmalang.dogma.paramExpected("resp", resp, null);_dogmalang.dogma.paramExpected("logger", logger, null);{
          let msg;if (opts != null ? opts.resource : null) {
            msg = (0, _dogmalang.fmt)("%s %s %s (from %s) HTTP/%s %s %s", req.client.addr, req.http.method, req.path, req.resource.fullPath, req.http.version, resp.status(), (0, _dogmalang.datetime)() - resp._ts);
          } else {
            msg = (0, _dogmalang.fmt)("%s %s %s HTTP/%s %s %s", req.client.addr, req.http.method, req.path, req.http.version, resp.status(), (0, _dogmalang.datetime)() - resp._ts);
          }logger.info("HTTP", msg);
        }
      } };
  }
}module.exports = exports = plugin;