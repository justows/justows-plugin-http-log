"use strict";

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const assert = _dogmalang.dogma.use(require("justo.assert"));const plugin = _dogmalang.dogma.use(require("../../../justows.plugin.http.log"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.test)("pre", () => {
      {
        const resp = {};plugin().pre(resp);assert(resp._ts).is("Date");
      }
    });(0, _justo.suite)("post", () => {
      {
        let data;let req;let resp;const logger = { ["info"]: (src, msg) => {
            _dogmalang.dogma.paramExpected("src", src, null);_dogmalang.dogma.paramExpected("msg", msg, null);{
              data = [src, msg];
            }
          } };(0, _justo.init)("*", () => {
          {
            req = { ["path"]: "/es/hello", ["client"]: { ["addr"]: "1.2.3.4" }, ["http"]: { ["method"]: "GET", ["version"]: "1.1" }, ["resource"]: { ["fullPath"]: "/svc/rsc" } };resp = { ["_ts"]: (0, _dogmalang.datetime)(), ["status"]: () => {
                {
                  return 200;
                }
              } };
          }
        });(0, _justo.test)("without resource", done => {
          _dogmalang.dogma.paramExpected("done", done, null);{
            setTimeout(() => {
              plugin().post(req, resp, logger);assert(data).isList().len(2);assert(_dogmalang.dogma.getItem(data, 0)).eq("HTTP");assert(_dogmalang.dogma.getItem(data, 1)).like("^1.2.3.4 GET /es/hello HTTP/1.1 200 [0-9]{4}$");done();
            }, 1250);
          }
        });(0, _justo.test)("with resource", done => {
          _dogmalang.dogma.paramExpected("done", done, null);{
            setTimeout(() => {
              plugin({ ["resource"]: true }).post(req, resp, logger);assert(data).isList().len(2);assert(_dogmalang.dogma.getItem(data, 0)).eq("HTTP");assert(_dogmalang.dogma.getItem(data, 1)).like("^1.2.3.4 GET /es/hello \\(from /svc/rsc\\) HTTP/1.1 200 [0-9]{4}$");done();
            }, 1250);
          }
        });
      }
    });
  }
}).tags("plugin");